import "../styles/globals.scss";
import "react-datetime/css/react-datetime.css";
import "react-toastify/dist/ReactToastify.css";

import store from "../redux/store";
import { Provider } from "react-redux";
import "moment/locale/vi";
import type { AppProps } from "next/app";
import { ApolloProvider } from "@apollo/client";
import client from "../graphql-client/client";
import Layout from "../components/layouts";
import { ToastContainer } from "react-toastify";

import { ChakraProvider } from "@chakra-ui/react";
import { extendTheme } from "@chakra-ui/react";
import moment from "moment";

const colors = {
  brand: {
    900: "#1a365d",
    800: "#153e75",
    700: "#2a69ac",
  },
};

const theme = extendTheme({ colors });

function MyApp({ Component, pageProps }: AppProps) {
  moment.locale("vi");
  return (
    <Provider store={store}>
      <ApolloProvider client={client}>
        <ToastContainer />
        <ChakraProvider theme={theme}>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </ChakraProvider>
      </ApolloProvider>
    </Provider>
  );
}

export default MyApp;
