import { Flex } from "@chakra-ui/react";

export default function FourOhFour() {
  return (
    <Flex
      justifyContent="center"
      justifyItems="center"
      flexWrap="wrap"
      flexDirection="column"
      alignContent="center"
    >
      <div
        style={{
          fontWeight: "900",
          background: "url(bg.jpg) no-repeat",
          WebkitBackgroundClip: "text",
          WebkitTextFillColor: "transparent",
          backgroundSize: "cover",
          backgroundPosition: "center",
          fontSize: "200px",
        }}
      >
        Oops!
      </div>
      <h1 style={{ textAlign: "center", fontSize: "40px", fontWeight: "bold" }}>
        404 - Page Not Found
      </h1>
    </Flex>
  );
}
