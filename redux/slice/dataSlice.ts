import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Todo } from "../../types";
import type { RootState } from "../store";

// Define a type for the slice state
interface DataSlice {
  moneys: [Todo];
}

// Define the initial state using that type
const initialState: DataSlice = {
  moneys: [],
};

export const dataSlice = createSlice({
  name: "data",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setMoneys: (state, action: PayloadAction<[Todo]>) => {
      state.moneys = action.payload;
    },
    addMoneys: (state, action: PayloadAction<any>) => {
      state.moneys.unshift(action.payload);
    },
  },
});

export const { setMoneys, addMoneys } = dataSlice.actions;

export default dataSlice.reducer;
