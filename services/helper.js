import { toast } from "react-toastify";

const helper = {};

helper.converMoney = (price) => {
  return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
};

// helper.converMoney = (ddmmyyyy) => {
//   return ddmmyyyy.
// }

helper.toast = (type, content) => {
  return toast[type](content, {
    position: "top-right",
    autoClose: 3000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
};

export default helper;
