import moment from "moment";

export interface Todo {
  readonly id: String;
  readonly sid: String;
  name: String;
  date: Date;
  deadline?: String;
  money?: Int32Array;
  readonly type: String;
  readonly userId: String;
}
