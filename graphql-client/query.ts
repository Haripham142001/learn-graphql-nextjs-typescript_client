import { gql } from "@apollo/client";

const getStudents = gql`
  query getStudentQuery {
    students {
      name
      id
    }
  }
`;

export { getStudents };
