import React from "react";
import SimpleSidebar from "./components/SimpleSidebar";

type Props = {
  children: JSX.Element;
};

const Layout = ({ children }: Props) => {
  return <SimpleSidebar>{children}</SimpleSidebar>;
};

export default Layout;
