import {
  Box,
  Button,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  useDisclosure,
} from "@chakra-ui/react";
import React, { Fragment, useState } from "react";
import { useForm } from "react-hook-form";
import Datetime from "react-datetime";
import { NumericFormat } from "react-number-format";
import { Todo } from "../../../types";
import moment from "moment";
import client from "../../../graphql-client/client";
import { gql } from "@apollo/client";
import helper from "../../../services/helper";
import { useAppDispatch } from "../../../redux/hook";
import { addMoneys } from "../../../redux/slice/dataSlice";

type Props = {};

interface InputData {
  time?: any;
  money?: Number;
}

const ModalComponent = (props: Props) => {
  const dispatch = useAppDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const finalRef = React.useRef(null);
  const { register, handleSubmit, control } = useForm();
  const [input, setInput] = useState<InputData>({
    time: new Date(),
  });

  const onSubmit = async (dataForm: any) => {
    const { data } = await client.mutate({
      mutation: gql`
        mutation CreateTodo(
          $userId: String!
          $date: String!
          $name: String!
          $type: String!
          $money: Int
        ) {
          createTodo(
            userId: $userId
            date: $date
            name: $name
            type: $type
            money: $money
          ) {
            name
            money
            date
          }
        }
      `,
      variables: {
        userId: "867470a9-b4ce-465b-bec4-59a362263f7c",
        name: dataForm.name,
        date: input.time,
        type: "spend",
        money: input?.money,
      },
    });
    if (data) {
      helper.toast("success", "Thêm thành công");
      onClose();
      dispatch(addMoneys(data.createTodo));
      setInput({
        time: new Date(),
      });
    }
  };

  return (
    <Fragment>
      <Stack direction="row" spacing={4}>
        <Button colorScheme="pink" variant="solid" onClick={onOpen}>
          Thêm
        </Button>
      </Stack>
      <Modal finalFocusRef={finalRef} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Tạo mới chi tiêu</ModalHeader>
          <ModalCloseButton />
          <form onSubmit={handleSubmit(onSubmit)}>
            <ModalBody>
              <Input placeholder="Tên" {...register("name")} />
              <Datetime
                onChange={(e) => setInput({ ...input, time: e })}
                value={input?.time || new Date()}
              />
              <div className="numberinput">
                <NumericFormat
                  thousandSeparator=","
                  onChange={(e) =>
                    setInput({
                      ...input,
                      money: Number(e.target.value.replaceAll(",", "")),
                    })
                  }
                  placeholder="Số tiền"
                />
              </div>
            </ModalBody>
            <ModalFooter>
              <Button colorScheme="blue" mr={3} type="submit">
                Tạo mới
              </Button>
            </ModalFooter>
          </form>
        </ModalContent>
      </Modal>
    </Fragment>
  );
};

export default ModalComponent;
