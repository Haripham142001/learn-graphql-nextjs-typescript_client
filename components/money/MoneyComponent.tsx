import React from "react";
import {
  TableContainer,
  Table,
  TableCaption,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
  Tfoot,
  Box,
  IconButton,
} from "@chakra-ui/react";
import { FiDelete, FiEdit } from "react-icons/fi";
import { Todo } from "../../types";
import helper from "../../services/helper";
import moment from "moment";
import ModalComponent from "./components/ModalComponent";

type Props = {
  data: [Todo];
};

const MoneyComponent = ({ data }: Props) => {
  return (
    <Box bg="white" w="100%" p={4} borderRadius="md">
      <ModalComponent />
      <TableContainer>
        <Table variant="simple">
          <TableCaption>Imperial to metric conversion factors</TableCaption>
          <Thead>
            <Tr>
              <Th style={{ width: "5%" }}>STT</Th>
              <Th style={{ width: "50%" }}>Nội dung</Th>
              <Th style={{ width: "20%" }}>Số tiền</Th>
              <Th style={{ width: "15%" }}>Ngày</Th>
              <Th>Thao tác</Th>
            </Tr>
          </Thead>
          <Tbody>
            {data.map((row, index) => {
              return (
                <Tr
                  key={row.sid + ""}
                  color={Number(row?.money) > 0 ? "green" : "red"}
                >
                  <Td>{index}</Td>
                  <Td>{row?.name}</Td>
                  <Td>
                    {Number(row?.money) > 0 && "+"}
                    {helper.converMoney(row?.money) || 0}
                  </Td>
                  <Td>
                    {moment(new Date(Number(row.date)), "DDMMYYYY").format("L")}
                  </Td>
                  <Td>
                    <IconButton
                      colorScheme="green"
                      aria-label="Chỉnh sửa"
                      title="Chỉnh sửa"
                      size="md"
                      icon={<FiEdit />}
                    />
                    <IconButton
                      colorScheme="red"
                      aria-label="Xóa"
                      title="Xóa"
                      size="md"
                      icon={<FiDelete />}
                      ml="1"
                    />
                  </Td>
                </Tr>
              );
            })}
          </Tbody>
          <Tfoot></Tfoot>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default MoneyComponent;
